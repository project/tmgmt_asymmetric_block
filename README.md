## Translation Management Tool Asymmetric Block

### Introduction
Allows for the TMGMT module to create block clones per layout builder content
when they are translated.

### Requirements
The following modules listed below needs to be installed an configured.

 * Requires Drupal 9 or higher.
 * Installation of TMGMT module
 * Install and configure Layout Builder Asymmetric (layout_builder_at)

### Additional Information
 * The code base uses some of the patch code from
   https://www.drupal.org/project/tmgmt/issues/3097660 to allow for inline
   blocks to work with tmgmt.

