<?php

namespace Drupal\tmgmt_asymmetric_block\EventSubscriber;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\layout_builder\Field\LayoutSectionItemList;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;
use Drupal\layout_builder\Section;
use Drupal\layout_builder\SectionComponent;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent;
use Drupal\tmgmt_asymmetric_block\TmgmtAsymmetricBlockJobTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber Finished job item.
 */
class TmgmtJobSubscriber implements EventSubscriberInterface {

  use LayoutEntityHelperTrait;
  use TmgmtAsymmetricBlockJobTrait;
  use StringTranslationTrait;

  /**
   * The collection revision name.
   */
  const REVISION_COLLECTION_NAME = 'tmgmt_asymmetric_block.entity.revision';

  /**
   * The user interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The temp store repository interface.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $sharedTempStore;

  /**
   * The job item entity interface.
   *
   * @var \Drupal\tmgmt\JobItemInterface[]
   */
  protected $jobItems;

  /**
   * The source job item entity interface.
   *
   * @var \Drupal\tmgmt\JobItemInterface
   */
  protected $sourceJobItem;

  /**
   * The Job entity interface.
   *
   * @var \Drupal\tmgmt\JobInterface
   */
  protected $job;

  /**
   * The target language code id.
   *
   * @var string
   */
  protected $targetLangcode;

  /**
   * The source of the translated entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionableInterface
   */
  protected $sourceEntity;

  /**
   * The Uuid interface.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The translated entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Initializing services.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_temp_store
   *   The Shared temp store factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factor
   *   The logger factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    SharedTempStoreFactory $shared_temp_store,
    LoggerChannelFactoryInterface $logger_factor,
    UuidInterface $uuid_service,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->sharedTempStore = $shared_temp_store;
    $this->logger = $logger_factor->get('tmgmt');
    $this->sharedTempStore = $shared_temp_store;
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Static class constant => method on this class.
      TmgmtJobEvent::FINISHED => ['onFinished'],
      TmgmtJobEvent::DELETED => ['onDeleted'],
      TmgmtJobEvent::PRESAVE => ['onPresave'],
    ];
  }

  /**
   * Removed the orphan temp revision id from job being deleted.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent $event
   *   The job event.
   */
  public function onDeleted(TmgmtJobEvent $event):void {
    $this->initialized($event);
    $this->processCleanUp($event);
  }

  /**
   * Handles a presave event when a job has finished.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent $event
   *   The job event.
   */
  public function onFinished(TmgmtJobEvent $event):void {
    $this->initialized($event);
    $this->handlePreSave($event);
    $this->processCleanUp($event);
  }

  /**
   * Assigned the job, job item and target language code to property.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent $event
   *   The job item interface.
   */
  public function initialized(TmgmtJobEvent $event):void {
    /** @var \Drupal\tmgmt\JobInterface $job */
    $job = $event->getJob();
    $this->job = $job;
    // Using the $job_items variable to avoid the array getting modified for
    // $this->jobItems.
    $job_items = array_reverse($job->getItems());
    // Get the very first array of the job item.
    $job_item = array_pop($job_items);
    // Get the current job item from the presave entity.
    $this->jobItems = $this->job->getItems();

    $this->sourceJobItem = $job_item;

    // The target language object to translate to.
    $this->targetLangcode = $this->job->getTargetLanguage()->getId();
  }

  /**
   * Gets the primary entity from the job.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|false
   *   Returns the untranslated entity or false if the entity is not found.
   */
  public function getEntityJobItem() {
    $entity_job_items = $this->entityTypeManager->getStorage('tmgmt_job_item')->loadByProperties([
      'tjid' => $this->sourceJobItem->getJobId(),
      'item_type' => $this->sourceJobItem->getItemType(),
    ]);
    if (!empty($entity_job_items)) {
      // Get the object from the first array key..
      /** @var \Drupal\tmgmt\JobItemInterface $entity_job_item */
      $entity_job_item = reset($entity_job_items);
      $entity = $this->entityTypeManager->getStorage($entity_job_item->getItemType())->load($entity_job_item->getItemId());
      if ($entity) {
        return $entity;
      }
    }
    return FALSE;
  }

  /**
   * Handles saving of the layout and its component.
   */
  public function handlePreSave() {
    $entity = $this->getEntityJobItem();
    if (!$entity) {
      return;
    }
    if (!$this->isLayoutCompatibleEntity($entity)) {
      return;
    }

    // Get the source entity.
    $this->setSourceEntity($entity);
    $translated_entity = $this->getTranslatedEntity();

    $this->updateSectionComponents($translated_entity);
  }

  /**
   * Update the each components in the section for layout builder translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $translated_entity
   *   The translated entity interface.
   *
   *   Save the entity event if no block has been updated, mainly use on
   *   updating the main entity.  Defaults is FALSE.
   */
  public function updateSectionComponents(ContentEntityInterface $translated_entity) {
    $blockUpdated = FALSE;
    $targetLangcode = $this->job->getTargetLangcode();
    $translated_entity
      ->get(OverridesSectionStorage::FIELD_NAME)
      ->setLangcode($targetLangcode);

    if ($this->resetTranslatedRegion()) {
      $translated_entity->set(OverridesSectionStorage::FIELD_NAME, []);
    }

    $this->entity = $translated_entity;
    /** @var \Drupal\layout_builder\Field\LayoutSectionItemList|null $translated_layout_field */
    $translated_layout_field = $translated_entity->get(OverridesSectionStorage::FIELD_NAME);

    if (!$translated_layout_field) {
      return;
    }

    /** @var \Drupal\layout_builder\SectionStorageInterface $section_storage */
    $section_storage = $this->getSectionStorageForEntity($translated_entity);
    $uuid = [];
    if ($section_storage) {

      $translated_section = $translated_layout_field->getSections();
      /** @var \Drupal\layout_builder\SectionListInterface $sections */
      $sections = $this->getSourceEntity()->get(OverridesSectionStorage::FIELD_NAME);
      foreach ($sections->getSections() as $delta => $section) {
        $settings = $section->getLayoutSettings();
        // Need to remap the uuid if they have change in nested layouts.
        if (!empty($settings['section_uid'])) {
          $uuid[$settings['section_uid']] = $delta;
        }
        $layout_definition = $section->getLayout()->getPluginDefinition();
        if (empty($translated_section[$delta]) ||
          $section->getLayoutId() !== $translated_section[$delta]->getLayoutId()) {
          $cloned_sections[$delta] = clone $section;
          foreach ($cloned_sections[$delta]->getComponents() as $component) {
            $cloned_sections[$delta]->removeComponent($component->getUuid());
          }
          if (!empty($translated_section[$delta]) && $section->getLayoutId() !== $translated_section[$delta]->getLayoutId()) {
            // If the layout section is different, we want to remove and readd
            // no new section due to components can be place in new layout
            // sections.
            $translated_layout_field->removeSection($delta);
          }
          $translated_layout_field->insertSection($delta, $cloned_sections[$delta]);
        }
        else {
          // Nuke everything and refresh the components since there is no way
          // to reference the replace component.
          foreach ($translated_section[$delta]->getComponents() as $component) {
            $this->removeComponent($translated_section[$delta], $component);
            $translated_section[$delta]->removeComponent($component->getUuid());
          }
        }
        foreach ($layout_definition->getRegionNames() as $region) {
          $components = $section->getComponentsByRegion($region);
          foreach ($components as $component) {
            $cloned_component = clone $component;
            foreach ($this->jobItems as $id => $jobItem) {
              $section_component_config = $cloned_component->get('configuration');
              if ($this->canUpdateComponent($section_component_config, $jobItem->getItemType())) {
                $updated = $this->updateComponent($section_storage, $cloned_component, $translated_layout_field, $delta, $jobItem);
                if (!empty($updated)) {
                  // Remove the job items so that it does not need to be loop
                  // again on the next run.
                  unset($this->jobItems[$id]);
                  $blockUpdated = TRUE;
                }
              }
            }
          }
        }
      }
    }
    if ($blockUpdated) {
      if (!empty($uuid)) {
        $this->updateLayoutReferenceUuid($translated_layout_field, $uuid);
      }
      $this->saveEntity($translated_entity, $translated_layout_field);
    }
  }

  /**
   * Remap the sub section uuid with the new one.
   *
   * @param \Drupal\layout_builder\Field\LayoutSectionItemList $translated_layout_field
   *   The updated layout sections with new changes.
   * @param array $uuid
   *   The deltas uuid to map.
   */
  public function updateLayoutReferenceUuid(LayoutSectionItemList $translated_layout_field, array $uuid) {
    foreach ($translated_layout_field->getSections() as $section) {
      $settings = $section->getLayoutSettings();
      if (!empty($settings['sub_section']['section_uid'])) {
        $translated_uuid = $settings['sub_section']['section_uid'];
        if (isset($uuid[$translated_uuid])) {
          $delta = $uuid[$translated_uuid];
          $settings['sub_section']['section_uid'] = $translated_layout_field
            ->getSection($delta)
            ->getLayoutSettings()['section_uid'];
          $section->setLayoutSettings($settings);
        }
      }
    }
  }

  /**
   * Remove component from section.
   *
   * @param \Drupal\layout_builder\Section $section
   *   The section of layout builder.
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The section component.
   */
  public function removeComponent(Section $section, SectionComponent $component) {
    $section->removeComponent($component->getUuid());
  }

  /**
   * Save the entity with any layout changes.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $translated_entity
   *   The translated entity.
   * @param \Drupal\layout_builder\Field\LayoutSectionItemList $translated_layout_field
   *   The updated layout sections with new changes.
   */
  public function saveEntity(ContentEntityInterface $translated_entity, LayoutSectionItemList $translated_layout_field) {
    $sections = !$translated_layout_field->isEmpty()
      ? $translated_layout_field->getSections()
      : [];

    /** @var \Drupal\Core\Entity\TranslatableRevisionableStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($translated_entity->getEntityTypeId());
    $source_entity = $this->getSourceEntity();
    $revision_key = implode('.', [
      $source_entity->getEntityTypeId(),
      $source_entity->id(),
      $source_entity->getLoadedRevisionId(),
      $this->targetLangcode,
    ]);
    $temp_store = $this->sharedTempStore->get($this::REVISION_COLLECTION_NAME);
    $translated_entity = $storage->createRevision($translated_entity, FALSE);
    /** @var \Drupal\Core\Entity\ContentEntityInterface $translated_entity */
    $translated_entity->set(OverridesSectionStorage::FIELD_NAME, $sections);
    $moderation_info = \Drupal::service('content_moderation.moderation_information');
    $workflow = $moderation_info->getWorkflowForEntity($this->getSourceEntity());
    $moderation_state = \Drupal::config('tmgmt_content.settings')->get('default_moderation_states.' . $workflow->id());
    if ($moderation_state) {
      $translated_entity->set('moderation_state', $moderation_state);
    }

    if ($translated_entity instanceof RevisionLogInterface) {
      $translated_entity->setRevisionLogMessage($this->t('Created by translation job for blocks to <a href=":url">@label</a>.', [
        ':url' => $translated_entity->toUrl()->toString(),
        '@label' => $translated_entity->label(),
      ]));
    }

    $translated_entity->save();
    // If there is revision created, store that very first revision created.
    // This will be used to reference later on to get the previous components.
    $temp_store->set($revision_key, ['revision_id' => $translated_entity->getRevisionId()]);

    // Since content in layout is being updated in code, there will not be
    // a revert option, we want to cleanup any stale tempstore for that entity.
    $collection = 'layout_builder.section_storage.overrides';
    $key = implode('.', [
      $translated_entity->getEntityTypeId(),
      $translated_entity->id(),
      // The view mode to clear.
      'default',
      $this->targetLangcode,
    ]);
    $tempstore = $this->sharedTempStore->get($collection, $key);
    if ($tempstore) {
      $tempstore->delete($key);
    }

    // Do not reset the sections now that its already been save with the new
    // sorted region.
    $this->resetTranslatedRegion(FALSE);
  }

  /**
   * Set the value of the component before it is updated.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage interface.
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The component in the layout section.
   * @param \Drupal\layout_builder\Field\LayoutSectionItemList $translated_layout_field
   *   The translation override section list.
   * @param int $delta
   *   The delta of the source section.
   * @param \Drupal\tmgmt\JobItemInterface $jobItem
   *   The job item data.
   *
   * @return bool
   *   Returns TRUE if the component is added to the translated layout section.
   */
  public function updateComponent(
    SectionStorageInterface $section_storage,
    SectionComponent $component,
    LayoutSectionItemList $translated_layout_field,
    int $delta,
    JobItemInterface $jobItem,
  ): bool {
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $block_storage */
    $block_storage = $this->entityTypeManager->getStorage($jobItem->getItemType());
    $data = $jobItem->getData();
    // Load current block and get the block ID.
    /** @var \Drupal\layout_builder\Plugin\Block\InlineBlock $plugin */
    $plugin = $component->getPlugin();
    $configuration = $plugin->getConfiguration();
    if ($this->isCustomBlock($configuration, 'block_content')) {
      $section = $translated_layout_field->getSection($delta);
      // Reused the same configuration.
      if (!empty($data['info'][0]['value']['#translation']['#text'])) {
        $configuration['label'] = $data['info'][0]['value']['#translation']['#text'];
        $component->setConfiguration($configuration);
      }
      $this->addComponentToSection($section, $component, FALSE);
      return TRUE;
    }
    if (!empty($data['info'][0]['value']['#translation']['#entity_created'])) {
      $entity_created = $data['info'][0]['value']['#translation']['#entity_created'];
      $translated_block_revision = $block_storage->loadRevision($entity_created['#revision_id']);
      /** @var \Drupal\block_content\BlockContentInterface $block_content_revision */
      \Drupal::service('inline_block.usage')->addUsage($translated_block_revision->id(), $this->getEntity());
      $block_content_revision = $translated_block_revision;

      $configuration['block_revision_id'] = (string) $entity_created['#revision_id'];
      $configuration['label'] = $block_content_revision->label();
      $section = $translated_layout_field->getSection($delta);
      $component->setConfiguration($configuration);
      $this->addComponentToSection($section, $component);
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Allow to update the component.
   *
   * @param array $section_component_config
   *   The section configuration component.
   * @param string $entity_type
   *   The entity type of the block component.
   *
   * @return bool
   *   Returns true for the block component.
   */
  public function canUpdateComponent(array $section_component_config, string $entity_type) {
    // Only update the component inline blocks.
    $is_inline_block = !empty($section_component_config['block_revision_id']) && $entity_type === 'block_content';
    return $is_inline_block || $this->isCustomBlock($section_component_config, $entity_type);
  }

  /**
   * Get the translated entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Returns the translated entity interface.
   */
  public function getEntity() {
    return $this->entity;
  }

  /**
   * Check if the block is an inline block or custom block.
   *
   * @param array $section_component_config
   *   The section configuration component.
   * @param string $entity_type
   *   The entity type of the block component.
   *
   * @return bool
   *   Returns true for the block component.
   */
  public function isCustomBlock(array $section_component_config, string $entity_type) {
    return $entity_type === 'block_content'
      && !empty($section_component_config['id'])
      && str_contains($section_component_config['id'], 'block_content:');
  }

  /**
   * Add component to section.
   *
   * @param \Drupal\layout_builder\Section $section
   *   A section of the layout.
   * @param \Drupal\layout_builder\SectionComponent $component
   *   The section component.
   * @param bool $generate_uuid
   *   Generate a new uuid for the component, if a component is reusable
   *   this should be false.
   */
  public function addComponentToSection(Section $section, SectionComponent $component, $generate_uuid = TRUE) {
    if ($generate_uuid) {
      $uuid = $this->uuidService->generate();
      $component->set('uuid', $uuid);
    }
    else {
      $uuid = $component->getUuid();
    }
    $weight = $component->getWeight();
    $section->appendComponent($component);
    $section->getComponent($uuid)->setWeight($weight);
  }

  /**
   * Set the source entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity interface.
   */
  public function setSourceEntity(ContentEntityInterface $entity) {
    $this->sourceEntity = $this->getLatestAffectedRevisionEntity($entity);
  }

  /**
   * Get the latest revision source revision translation entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity interface.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionableInterface
   *   The source affected content entity interface.
   */
  public function getLatestAffectedRevisionEntity(ContentEntityInterface $entity) {
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    $latest_affected_revision_id = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $this->job->getSourceLanguage()->getId());
    if ($latest_affected_revision_id && $latest_affected_revision_id != $entity->getRevisionId()) {
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      $entity = $storage->loadRevision($latest_affected_revision_id);
    }
    return $entity;
  }

  /**
   * Create a new revision of the translated entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   The translated entity.
   */
  public function getTranslatedEntity() {
    $langcode = $this->targetLangcode;
    $entity = $this->getSourceEntity();
    /** @var \Drupal\Core\Entity\TranslatableRevisionableStorageInterface $entity_storage */
    $entity_storage = $this
      ->entityTypeManager
      ->getStorage($entity->getEntityTypeId());
    $revision_key = implode('.', [
      $entity->getEntityTypeId(),
      $entity->id(),
      $entity->getLoadedRevisionId(),
      $langcode,
    ]);

    $temp_store = $this->sharedTempStore->get($this::REVISION_COLLECTION_NAME);
    if ($store_data = $temp_store->get($revision_key)) {
      // Check to see if this entity was part of an existing process that had
      // other blocks added, if so load those that entity with that translation.
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity_loaded */
      $entity_loaded = $entity_storage->loadRevision($store_data['revision_id']);
      if ($entity_loaded) {
        return $entity_loaded->getTranslation($langcode);
      }
    }

    if ($entity->isTranslatable()) {
      // Add a new translation if none existed.
      // Check latest Entity for all the label data.
      $latest_entity_id = $entity_storage->getLatestRevisionId($entity->id());
      if ($latest_entity_id) {
        /** @var \Drupal\Core\Entity\ContentEntityInterface $latest_entity */
        $latest_entity = $entity_storage->loadRevision($latest_entity_id);
        if ($latest_entity && $latest_entity->hasTranslation($langcode)) {
          $translation = $latest_entity->getTranslation($langcode);
          return $translation;
        }
      }
      if (!$entity->hasTranslation($langcode)) {
        $title = $entity->label();
        $translation = $entity->addTranslation($langcode, $entity->toArray());
        // Set any required field on the entity before saving.
        $label_key = $entity->getEntityType()->getKeys()['label'];
        $translation->set($label_key, $title);
      }
    }

    return $translation;
  }

  /**
   * Get the source entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|\Drupal\Core\Entity\RevisionableInterface
   *   Get the source entity.
   */
  public function getSourceEntity() {
    return $this->sourceEntity;
  }

  /**
   * Process to delete the revision id from shared temp storage.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent $event
   *   The job event.
   */
  public function processCleanUp(TmgmtJobEvent $event) {
    // Get the entity used from the job item.
    // Get the very first array of the job item.
    $job_item = $this->sourceJobItem;
    if (!$job_item) {
      return;
    }
    $bundle = $job_item->getItemType();
    /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
    $entity = \Drupal::entityTypeManager()->getStorage($bundle)->load($job_item->getItemId());

    if ($entity) {
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
      $storage = $this->entityTypeManager->getStorage($bundle);
      $latest_affected_revision_id = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $this->job->getSourceLanguage()->getId());
    }
    if (!empty($latest_affected_revision_id)) {
      // Clean up the job tempstore after it has been completed.
      $entity = $storage->loadRevision($latest_affected_revision_id);
      /** @var \Drupal\Core\Entity\ContentEntityInterface $entity */
      if ($entity) {
        $this->cleanUpSharedRevisionEntity($entity);
      }
    }
  }

  /**
   * Clean up the temp shared storage for the last revision recorded.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity interface.
   */
  public function cleanUpSharedRevisionEntity(ContentEntityInterface $entity) {
    $revision_key = implode('.', [
      $entity->getEntityTypeId(),
      $entity->id(),
      $entity->getLoadedRevisionId(),
      $this->job->getTargetLanguage()->getId(),
    ]);

    $temp_store = $this->sharedTempStore->get('tmgmt_asymmetric_block.entity.revision');
    if ($temp_store->get($revision_key)) {
      $temp_store->delete($revision_key);
    }
  }

  /**
   * Set undefined language to default language.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobEvent $event
   *   The job event.
   */
  public function onPresave(TmgmtJobEvent $event) {
    /** @var \Drupal\tmgmt\JobInterface $job */
    $job = $event->getJob();

    $langcode = $job->getSourceLangcode();
    // If language is not defined, call back to the default language.
    if ($langcode === LanguageInterface::LANGCODE_NOT_SPECIFIED) {
      $default_language = \Drupal::service('language.default')
        ->get()
        ->getId();
      $job->set('source_language', $default_language);
    }
  }

  /**
   * Reset the translated to an empty section ie override instead of merged.
   *
   * Reset the layout builder sections the very first time so that
   * any shifted content weight will not have to be recalculated.
   *
   * @param bool $reset
   *   Reset to an empty layout for the current translation.  Defaults to TRUE.
   *
   * @return bool
   *   Reset layout builder if TRUE, else fetch existing translated regions and
   *   tries to merged in data.
   */
  public function resetTranslatedRegion(bool $reset = TRUE): bool {
    static $shouldReset = TRUE;
    if ($shouldReset && !$reset) {
      $shouldReset = FALSE;
    }
    return $shouldReset;
  }

}
