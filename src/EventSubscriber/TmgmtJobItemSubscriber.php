<?php

namespace Drupal\tmgmt_asymmetric_block\EventSubscriber;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\TempStore\SharedTempStoreFactory;
use Drupal\layout_builder\LayoutEntityHelperTrait;
use Drupal\tmgmt_asymmetric_block\Events\TmgmtJobItemEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber to add the inline block to layout builder.
 */
class TmgmtJobItemSubscriber implements EventSubscriberInterface {

  use LayoutEntityHelperTrait;

  /**
   * The collection revision name.
   */
  const REVISION_COLLECTION_NAME = 'tmgmt_asymmetric_block.entity.revision';

  /**
   * The user interface.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The target language code id.
   *
   * @var string
   */
  protected $targetLangcode;

  /**
   * The temp store repository interface.
   *
   * @var \Drupal\Core\TempStore\SharedTempStoreFactory
   */
  protected $sharedTempStore;

  /**
   * The Uuid interface.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidService;


  /**
   * The translated entity.
   *
   * @var \Drupal\Core\Entity\ContentEntityInterface
   */
  protected $entity;

  /**
   * Initializing services.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager interface.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factor
   *   The logger factory.
   * @param \Drupal\Core\TempStore\SharedTempStoreFactory $shared_temp_store
   *   The Shared temp store factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid_service
   *   The UUID interface.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactoryInterface $logger_factor,
    SharedTempStoreFactory $shared_temp_store,
    UuidInterface $uuid_service
    ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger_factor->get('tmgmt');
    $this->sharedTempStore = $shared_temp_store;
    $this->uuidService = $uuid_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Static class constant => method on this class.
      TmgmtJobItemEvent::ACCEPTED => ['onAccepted'],
    ];
  }

  /**
   * Handles the presave entity when a job item is accepted.
   *
   * @param \Drupal\tmgmt_asymmetric_block\Events\TmgmtJobItemEvent $event
   *   The job item interface.
   */
  public function onAccepted(TmgmtJobItemEvent $event) {
    // Do logic for any job item accepted.
  }

}
