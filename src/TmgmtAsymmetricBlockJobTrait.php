<?php

namespace Drupal\tmgmt_asymmetric_block;

/**
 * Traits used for XTM and TMGMT.
 */
trait TmgmtAsymmetricBlockJobTrait {

  /**
   * Get the node ID by the job id.
   *
   * @param int $job_id
   *   The job ID.
   *
   * @return int|false
   *   Return the node ID or false if none existed for that job ID that has
   *   a pending translation.
   */
  public function getNodeIdByJobId($job_id) {
    return \Drupal::database()->select('tmgmt_job_item', 't')
      ->fields('t', ['item_id'])
      ->condition('tjid', $job_id)
      ->condition('item_type', 'node')
      ->condition('plugin', 'content')
      ->execute()
      ->fetchField();
  }

}
