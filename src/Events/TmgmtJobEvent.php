<?php

namespace Drupal\tmgmt_asymmetric_block\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\tmgmt\JobInterface;

/**
 * Event fire on a node entity.
 */
class TmgmtJobEvent extends Event {
  const FINISHED = 'tmgmt.job_finished';
  const DELETED = 'tmgmt.job_deleted';
  const PRESAVE = 'tmgmt.job.presave';

  /**
   * The entity interface.
   *
   * @var \Drupal\tmgmt\JobInterface
   */
  public $job;

  /**
   * Constructs the object.
   *
   * @param \Drupal\tmgmt\JobInterface $entity
   *   The account of the user logged in.
   */
  public function __construct(JobInterface $entity) {
    $this->job = $entity;
  }

  /**
   * Get the Entity.
   *
   * @return \Drupal\tmgmt\JobItemInterface
   *   The Job item entity.
   */
  public function getJob() {
    return $this->job;
  }

}
