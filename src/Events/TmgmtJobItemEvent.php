<?php

namespace Drupal\tmgmt_asymmetric_block\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\tmgmt\JobItemInterface;

/**
 * Event fire on a node entity.
 */
class TmgmtJobItemEvent extends Event {
  const ACCEPTED = 'tmgmt.job_item_accepted';

  /**
   * The JobItem interface.
   *
   * @var \Drupal\tmgmt\JobItemInterface
   */
  protected $jobItem;

  /**
   * Constructs the object.
   *
   * @param \Drupal\tmgmt\JobItemInterface $entity
   *   The account of the user logged in.
   */
  public function __construct(JobItemInterface $entity) {
    $this->jobItem = $entity;
  }

  /**
   * Get the Entity.
   *
   * @return \Drupal\tmgmt\JobItemInterface
   *   The Job item entity.
   */
  public function getJobItem() {
    return $this->jobItem;
  }

}
