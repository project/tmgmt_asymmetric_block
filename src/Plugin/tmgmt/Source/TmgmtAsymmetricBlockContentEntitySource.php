<?php

namespace Drupal\tmgmt_asymmetric_block\Plugin\tmgmt\Source;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\tmgmt\JobItemInterface;
use Drupal\tmgmt_content\Plugin\tmgmt\Source\ContentEntitySource;
use Drupal\Core\Render\Element;
use Drupal\Core\Entity\RevisionLogInterface;
use Drupal\Core\Entity\TranslatableRevisionableStorageInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\layout_builder\Plugin\SectionStorage\OverridesSectionStorage;

/**
 * Content entity source plugin controller for handling asymmetric translation.
 */
class TmgmtAsymmetricBlockContentEntitySource extends ContentEntitySource {

  /**
   * Saves translation data in an entity translation.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity for which the translation should be saved.
   * @param array $data
   *   The translation data for the fields.
   * @param string $target_langcode
   *   The target language.
   * @param \Drupal\tmgmt\JobItemInterface $item
   *   The job item.
   * @param bool $save
   *   (optional) Whether to save the translation or not.
   *
   * @throws \Exception
   *   Thrown when a field or field offset is missing.
   */
  protected function doSaveTranslations(ContentEntityInterface $entity, array $data, $target_langcode, JobItemInterface $item, $save = TRUE) {
    $original_entity = clone $entity;
    if ($this->canBeDuplicated($entity)) {
      // Create duplicate of the entity.
      $entity = $entity->createDuplicate();
    }
    // If the translation for this language does not exist yet, initialize it.
    if (!$entity->hasTranslation($target_langcode)) {
      $entity->addTranslation($target_langcode, $entity->toArray());
    }

    $translation = $entity->getTranslation($target_langcode);
    $manager = \Drupal::service('content_translation.manager');
    if ($manager->isEnabled($translation->getEntityTypeId(), $translation->bundle())) {
      $manager->getTranslationMetadata($translation)->setSource($entity->language()->getId());
    }

    foreach (Element::children($data) as $field_name) {
      $field_data = $data[$field_name];

      if (!$translation->hasField($field_name)) {
        throw new \Exception("Field '$field_name' does not exist on entity " . $translation->getEntityTypeId() . '/' . $translation->id());
      }

      $field = $translation->get($field_name);
      $field_processor = $this->getFieldProcessor($field->getFieldDefinition()->getType());
      $field_processor->setTranslations($field_data, $field);
    }

    $embeddable_fields = static::getEmbeddableFields($entity);
    foreach ($embeddable_fields as $field_name => $field_definition) {

      if (!isset($data[$field_name])) {
        continue;
      }

      $field = $translation->get($field_name);
      $target_type = $field->getFieldDefinition()->getFieldStorageDefinition()->getSetting('target_type');
      $is_target_type_translatable = $manager->isEnabled($target_type);
      // In case the target type is not translatable, the referenced entity will
      // be duplicated. As a consequence, remove all the field items from the
      // translation, update the field value to use the field object from the
      // source language.
      if (!$is_target_type_translatable) {
        $field = clone $entity->get($field_name);

        if (!$translation->get($field_name)->isEmpty()) {
          $translation->set($field_name, NULL);
        }
      }

      foreach (Element::children($data[$field_name]) as $delta) {
        $field_item = $data[$field_name][$delta];
        foreach (Element::children($field_item) as $property) {
          // Find the referenced entity. In case we are dealing with
          // untranslatable target types, the source entity will be returned.
          if ($target_entity = $this->findReferencedEntity($field, $field_item, $delta, $property, $is_target_type_translatable)) {
            if ($is_target_type_translatable) {
              // If the field is an embeddable reference and the property is a
              // content entity, process it recursively.
              // If the field is ERR and the target entity supports
              // the needs saving interface, do not save it immediately to avoid
              // creating two versions when content moderation is used but just
              // ensure it will be saved.
              $target_save = TRUE;
              if ($field->getFieldDefinition()->getType() == 'entity_reference_revisions' && $target_entity instanceof \Drupal\entity_reference_revisions\EntityNeedsSaveInterface) {
                $target_save = FALSE;
                $target_entity->needsSave();
              }

              $this->doSaveTranslations($target_entity, $field_item[$property], $target_langcode, $item, $target_save);
            }
            else {
              $duplicate = $this->createTranslationDuplicate($target_entity, $target_langcode);
              // Do not save the duplicate as it's going to be saved with the
              // main entity.
              $this->doSaveTranslations($duplicate, $field_item[$property], $target_langcode, $item, FALSE);
              $translation->get($field_name)->set($delta, $duplicate);
            }
          }
        }
      }
    }

    if (static::isModeratedEntity($translation)) {
      // Use the given moderation status if set. Otherwise, fallback to the
      // configured one in TMGMT settings.
      if (isset($data['#moderation_state'][0])) {
        $moderation_state = $data['#moderation_state'][0];
      }
      else {
        $moderation_info = \Drupal::service('content_moderation.moderation_information');
        $workflow = $moderation_info->getWorkflowForEntity($entity);
        $moderation_state = \Drupal::config('tmgmt_content.settings')->get('default_moderation_states.' . $workflow->id());
      }
      if ($moderation_state) {
        $translation->set('moderation_state', $moderation_state);
      }
    }
    // Otherwise, try to set a published status.
    elseif (isset($data['#published'][0]) && $translation instanceof EntityPublishedInterface) {
      if ($data['#published'][0]) {
        $translation->setPublished();
      }
      else {
        $translation->setUnpublished();
      }
    }

    if ($entity->getEntityType()->isRevisionable()) {
      /** @var \Drupal\Core\Entity\TranslatableRevisionableStorageInterface $storage */
      $storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());

      if ($storage instanceof TranslatableRevisionableStorageInterface) {
        // Always create a new revision of the translation.
        $new_revision_is_default = FALSE;
        if ($translation->getEntityTypeId() !== 'node') {
          $new_revision_is_default = $translation->isDefaultRevision();
        }

        $translation = $storage->createRevision($translation, $new_revision_is_default);

        if ($translation instanceof RevisionLogInterface) {
          $translation->setRevisionLogMessage($this->t('Created by translation job <a href=":url">@label</a>.', [
            ':url' => $item->getJob()->toUrl()->toString(),
            '@label' => $item->label(),
          ]));
        }
      }
    }

    if ($save) {
      $translation = $this->save($translation, $target_langcode, $item);
      // Store additional data as part of the entity created.
      $this->afterSave($translation, $target_langcode, $item, $original_entity);
    }
  }

  /**
   * Save the translation entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The translated entity.
   * @param string $target_langcode
   *   The target language.
   * @param \Drupal\tmgmt\JobItemInterface $item
   *   The job item.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *   Return the save translated entity after save.
   */
  public function save(ContentEntityInterface $entity, string $target_langcode, JobItemInterface $item) {
    // Convert the entity to an array and re-save it so that it doesn't create
    // a revision of the source being the main language.  This way it has its
    // own set of revision ID and ID, with the source language being the
    // targeted language.
    if ($this->canBeDuplicated($entity)) {
      $data = $entity->toArray();
      $data['default_langcode'][0]['value'] = TRUE;
      $data['content_translation_source'][0] = LanguageInterface::LANGCODE_NOT_SPECIFIED;
      $entity = $entity::create($data);
    }
    else {
      /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
      $storage = \Drupal::entityTypeManager()->getStorage($entity->getEntityTypeId());
      $has_language_revision = $storage->getLatestTranslationAffectedRevisionId($entity->id(), $target_langcode);
      if ($has_language_revision) {
        // If language is still in draft state, we want to update the content
        // within the instead of creating a new entity.
        $translation = $storage->loadRevision($has_language_revision);
        /** @var \Drupal\Core\Entity\ContentEntityInterface $translation */
        $translation = $translation->getTranslation($target_langcode);
        if ($translation->hasField(OverridesSectionStorage::FIELD_NAME)) {
          /** @var \Drupal\layout_builder\FieldLayoutSectionItemList $section_field */
          $section_field = $translation->get(OverridesSectionStorage::FIELD_NAME);
          $sections = $section_field->getSections();
          $entity->set(OverridesSectionStorage::FIELD_NAME, $sections);
        }
      }
      if ($entity->hasField(OverridesSectionStorage::FIELD_NAME)) {
        /** @var \Drupal\layout_builder\FieldLayoutSectionItemList $section_field */
        $section_field = $entity->get(OverridesSectionStorage::FIELD_NAME);
        // If there is layouts, if so set the sections it an empty node.
        if (!$section_field->getSections()) {
          $entity->set(OverridesSectionStorage::FIELD_NAME, []);
        }
      }
    }
    $entity->save();
    return $entity;
  }

  /**
   * Allow to capture the save translation entity and create the save entity.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $translation
   *   The translated entity.
   * @param string $target_langcode
   *   The target language.
   * @param \Drupal\tmgmt\JobItemInterface $item
   *   The job item.
   * @param \Drupal\Core\Entity\ContentEntityInterface $original_entity
   *   The entity source.
   */
  public function afterSave(ContentEntityInterface $translation, $target_langcode, JobItemInterface $item, ContentEntityInterface $original_entity) {
    // Allow for any other data to be stored.
    // Store the translated ids in the job item.
    $entity_type = $translation->getEntityType()->id();
    if ($this->canBeDuplicated($translation)) {
      $data['info'][0]['value']['#translation']['#entity_created'] = [
        '#type' => $entity_type,
        '#id' => $translation->id(),
        '#revision_id' => $translation->getRevisionId(),
        '#bundle' => $translation->bundle(),
        '#uuid' => $translation->uuid(),
        '#langcode' => $target_langcode,
        '#original_id' => $original_entity->id(),
        '#original_revision' => $original_entity->getRevisionId(),
      ];
      $item->updateData('info', $data['info']);
      $item->save();
    }
  }

  /**
   * Check if the entity should be translated.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity interface.
   *
   * @return bool
   *   Return true if entity type should be duplicated.
   */
  public function canBeDuplicated(ContentEntityInterface $entity) {
    // Check to see if the entity can be duplicated.
    // Override method to add any additional function, see the api docs for
    // hook_tmgmt_source_plugin_info_alter() in tmgmt module.
    $entity_type_ids = $this->getAllowableDuplicates();
    if (in_array($entity->getEntityType()->id(), $entity_type_ids)) {
      // Do not include custom blocks that is shared across blocks.
      if (!$entity->isReusable()) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Get the allowable content to be duplicated.
   *
   * @return array
   *   Return an array of the entity type id allow for duplications such as
   *   blocks.
   */
  public function getAllowableDuplicates() {
    return ['block_content'];
  }

}
