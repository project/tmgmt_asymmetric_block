<?php

namespace Drupal\tmgmt_asymmetric_block;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Register a new service if layout builder exists.
 */
class TmgmtAsymmetricBlockServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $modules = $container->getParameter('container.modules');
    if (isset($modules['layout_builder'])) {
      $container
        ->register('tmgmt_asymmetric_block.job_item_subscriber', 'Drupal\tmgmt_asymmetric_block\EventSubscriber\TmgmtJobItemSubscriber')
        ->addArgument(new Reference('entity_type.manager'))
        ->addArgument(new Reference('logger.factory'))
        ->AddArgument(new Reference('tempstore.shared'))
        ->AddArgument(new Reference('uuid'))
        ->addTag('event_subscriber');
    }
  }

}
